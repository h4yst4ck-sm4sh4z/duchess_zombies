import math
import telnetlib
import re
import time

HOST="zombies_8977dda19ee030d0ea35e97ad2439319.2014.shallweplayaga.me"
PORT=20689

class killZombies(object):
  def __init__(self):
    self.sock = None
    self.response = None
    self.zombie_x_angle = None
    self.zombie_y_angle = None
    self.puppy_x_angle = None
    self.puppy_y_angle = None
    self.time = None
    self.level = None
    self.pamount = 375
    self.ramount = 800
    self.rvelocity = 800
    self.pvelocity = 375
    self.distancebetween = None
    self.speed = None

  def distance(self, x,y):
    return math.sqrt(x*x+y*y)

  def fire_angle(self, x,y,v0):
    d = self.distance(x,y)
    t = d/v0
    y_prime = y+.5*9.81*t*t
    theta = math.atan(y_prime/x)
    theta = theta * 180/math.pi
    return (theta,x,y)
 
  def getitargetcords(self, x, y):
    x = int(x)
    y = int(y)
    d = self.distance(x,y)
    v = 0
    if d > 50:
      t = "r"
      v = self.rvelocity
    else:
      t = "p"
      v = self.pvelocity
    
    fa = self.fire_angle(x, y, v)
    return t+","+str(fa[0])+","+str(x)+","+str(y)


  def connect(self):
    self.sock = telnetlib.Telnet(HOST, PORT)
  
  def initarsenal(self):
    self.sock.read_until(b"3. H&K G3 w/ box magazines   800")
    self.sock.write("3\n") # choose rifle
    self.sock.read_until(b"3. Glock 17 9mm              375")
    self.sock.write("3\n") # choose pistol

  def sendShot(self):
    if (self.time):
      a = self.getitargetcords(int(self.distancebetween)/2+int(self.zombie_x_angle),self.zombie_y_angle)
      time.sleep(int(self.time)/2)
      self.sock.write(a+'\n')
      self.time = None
    else:
      a = self.getitargetcords(self.zombie_x_angle,self.zombie_y_angle)
      self.sock.write(a+'\n')

  def readNShot1(self):
    self.response = self.sock.read_until("Enter your shot details:")
    m = re.search('Level (\d+)', self.response)
    self.level = m.group(1)
    print self.level
    m = re.search('The zombie is stalking a puppy (\d+)m from your van and (\d+)m above your van', self.response) 
    self.zombie_x_angle = m.group(1)
    self.zombie_y_angle = m.group(2)
    print "X: " + repr(self.zombie_x_angle)
    print "Y: " + repr(self.zombie_y_angle) 
    self.sendShot()

  def readNShot2(self):
    self.response = self.sock.read_until("Enter your shot details:")
    print "#######"
    print self.response
    print "#######"
    m = re.search('Level (\d+)', self.response)
    self.level = m.group(1)
    print self.level
    m = re.search('The zombie is stalking a puppy across a hillside, starting at a distance of (\d+)m from your van and (\d+)m above it. The puppy is frozen in terror (\d+)m from your van and (\d+)m above your van.', self.response)
    self.zombie_x_angle = m.group(1)
    self.zombie_y_angle = m.group(2)
    print "X: " + repr(self.zombie_x_angle)
    print "Y: " + repr(self.zombie_y_angle)
    self.puppy_x_angle = m.group(3)
    self.puppy_y_angle = m.group(4)
    print "X: " + repr(self.puppy_x_angle)
    print "Y: " + repr(self.puppy_y_angle)
    m = re.search('You have (\d+) seconds', self.response)
    self.time = m.group(1)
  
    self.distancebetween = int(self.zombie_x_angle) - int(self.puppy_x_angle)
    if (int(self.distancebetween) < 0):  
      self.distancebetween = int(self.puppy_x_angle) - int(self.zombie_x_angle) 
    self.speed = int(self.distancebetween)/int(self.time)
    print self.distancebetween
    self.sendShot()


if __name__ == '__main__':
  z = killZombies()
  z.connect()
  z.initarsenal()
  for i in range(0,9):
    z.readNShot1()
 
  z.readNShot2()
  z.readNShot2()
